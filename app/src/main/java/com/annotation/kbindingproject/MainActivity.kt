package com.annotation.kbindingproject

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.keirin250.annotations.ActivityArguments
import com.keirin250.annotations.BindClick
import com.keirin250.annotations.Parameter

@ActivityArguments(
    Parameter(name = "contact", type = String::class),
    Parameter(name = "num", type = Int::class)
)
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        MainActivityBinding(this)

    }

    @BindClick(R.id.tv_hello)
    fun tvHelloClick() {
        ActivityNavigation.getUserActivityNavOptions(this, 10.0, 11.0)
            .setUser(User("a"))
            .navigateForResult(11)
    }
}