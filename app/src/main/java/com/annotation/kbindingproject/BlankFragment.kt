package com.annotation.kbindingproject


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.keirin250.annotations.BindClick
import com.keirin250.annotations.FragmentArguments
import com.keirin250.annotations.Parameter

/**
 * A simple [Fragment] subclass.
 */

@FragmentArguments(
    Parameter("user", User::class),
    Parameter("old", Int::class)
)
class BlankFragment : Fragment() {

    companion object {
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root: ViewGroup = inflater.inflate(R.layout.fragment_blank, container, false) as ViewGroup
        BlankFragmentBinding(this, root)
        return root
    }

    @BindClick(R.id.tv_in_fragment, false)
    fun tvInFragmentClick() {
    }


}
