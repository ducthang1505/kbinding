package com.annotation.kbindingproject

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.keirin250.annotations.ActivityArguments
import com.keirin250.annotations.BindClick
import com.keirin250.annotations.Parameter

@ActivityArguments(
    Parameter("user", User::class, true),
    Parameter("amount", Double::class),
    Parameter("fee", Double::class)
)
class UserActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)
        UserActivityBinding(this)
    }

    @BindClick(R.id.tv_user)
    fun tvUserClick() {
        ActivityNavigation.getMainActivityNavOptions(from = this, contact = "aa", num = 111)
            .clearTop().navigate()
    }
}
