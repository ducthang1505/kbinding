package com.annotation.kbindingproject

import com.keirin250.annotations.ActivityNavModule
import com.keirin250.annotations.FragmentProviderModule


/**
 * Created by Thang Tran on 2019-10-06.
 * Copyright © 2019 Neolab SI6. All rights reserved.
 */
@ActivityNavModule
object ActivityNavigation

@FragmentProviderModule
object FragmentProvider