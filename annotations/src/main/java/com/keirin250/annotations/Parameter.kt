package com.keirin250.annotations

import kotlin.reflect.KClass

@Retention(AnnotationRetention.SOURCE)
@Target(AnnotationTarget.VALUE_PARAMETER)
annotation class Parameter(
    val name: String,
    val type: KClass<out Any>,
    val nullable: Boolean = false
)