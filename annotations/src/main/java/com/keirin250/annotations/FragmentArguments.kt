package com.keirin250.annotations


/**
 * Created by Thang Tran on 2019-10-16.
 * Copyright © 2019 Neolab SI6. All rights reserved.
 */

@Retention(AnnotationRetention.SOURCE)
@Target(AnnotationTarget.CLASS)
annotation class FragmentArguments(vararg val args: Parameter)

@Retention(AnnotationRetention.SOURCE)
@Target(AnnotationTarget.CLASS)
annotation class FragmentProviderModule