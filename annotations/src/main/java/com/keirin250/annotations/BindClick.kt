package com.keirin250.annotations


/**
 * Created by Thang Tran on 2019-10-01.
 * Copyright © 2019 Neolab SI6. All rights reserved.
 */
@Retention(AnnotationRetention.SOURCE)
@Target(AnnotationTarget.FUNCTION)
annotation class BindClick(val viewId: Int, val hideKeyboardBefore: Boolean = true)