package com.keirin250.bindingprocess.generator

import com.google.auto.common.SuperficialValidation
import com.keirin250.annotations.FragmentArguments
import com.keirin250.annotations.FragmentProviderModule
import com.keirin250.annotations.Parameter
import com.keirin250.bindingprocess.*
import com.squareup.kotlinpoet.*
import java.io.Serializable
import javax.annotation.processing.ProcessingEnvironment
import javax.annotation.processing.RoundEnvironment
import javax.lang.model.element.TypeElement
import kotlin.reflect.KClass


/**
 * Created by Thang Tran on 2019-10-5.
 * Copyright © 2019 Neolab SI6. All rights reserved.
 */
class FragmentArgumentGenerator(roundEnv: RoundEnvironment, processingEnv: ProcessingEnvironment) :
    ArgumentsGenerator<FragmentArguments>(roundEnv, processingEnv) {

    override fun checkAndAddToList(): List<FileSpec> {
        checkProviderElement<FragmentProviderModule>()
        val fragmentAnnotations = roundEnv
            .getElementsAnnotatedWith(getAnnotationClass().java)
        if (fragmentAnnotations.size == 0) return mutableListOf()
        for (element in fragmentAnnotations) {
            if (!SuperficialValidation.validateElement(element)) continue
            checkIsComponent(ANDROID_SUPPORT_FRAGMENT, element)
            element as TypeElement
            //Check size parameter
            val args = checkAndGetParameters(element)
            for (arg in args) {
                addOrUpdateMap(element, getFunctionGetValue(element, arg))
            }
            addOrUpdateMap(element, buildProviderFunction(providerElement, element, args))
        }
        return generateFileSpecBuilder()
    }

    override fun getAnnotationClass(): KClass<out FragmentArguments> {
        return FragmentArguments::class
    }


    private fun buildProviderFunction(
        receiverElement: TypeElement,
        element: TypeElement,
        args: Array<out Parameter>
    ): FunSpec {
        val funName = "get${element.simpleName}Instance"
        val bundleClass = ANDROID_BUNDLE
        val functionBuilder = FunSpec.builder(funName)
            .receiver(
                receiverElement.asClassName(),
                CodeBlock.of("provider an instance of ${element.asClassName().simpleName}")
            )
        functionBuilder.returns(element.asClassName())
        val codeBlockBuilder = CodeBlock.builder()
            .add(" val fragment = %T()\n", element.asClassName())
            .add(" val args = %T()\n", bundleClass)

        for (arg in args) {
            val className = getClassName(arg)
            functionBuilder.addParameter(arg.name, className.copy(arg.nullable))
            val key = getArgumentKey(element, arg)

            val patternCode = when (className) {
                String::class.asTypeName(), Short::class.asTypeName(), Int::class.asTypeName(),
                Long::class.asTypeName(), Float::class.asTypeName(), Double::class.asTypeName(),
                Boolean::class.asTypeName(), Byte::class.asTypeName(), Char::class.asTypeName(),
                CharSequence::class.asTypeName() -> "put${className.simpleName}"

                else -> {
                    //Support Serializable and Parcelable
                    when {
                        className.getTypeElement(processingEnv).asType().isSubtypeOfType(
                            Serializable::class.java.canonicalName
                        ) ->
                            "putSerializable"
                        className.getTypeElement(processingEnv).isSubtypeOfType(ANDROID_PARCELABLE) ->
                            "putParcelable"
                        else -> throw IllegalArgumentException("Type $className not support by ActivityArguments annotation")
                    }
                }
            }

            val codeBlock = CodeBlock.of("args.$patternCode(%S, %L)\n", key, arg.name)
            codeBlockBuilder.add(codeBlock)
        }
        codeBlockBuilder.add("fragment.arguments = args\n")
        codeBlockBuilder.add("return fragment\n")
        functionBuilder.addCode(codeBlockBuilder.build())
        return functionBuilder.build()
    }

    override fun getFunctionGetValue(element: TypeElement, arg: Parameter): FunSpec {
        var functionName = "get${arg.name.capitalize()}"
        var className = getClassName(arg)
        val key = getArgumentKey(element, arg)
        val code: CodeBlock
        var nullable = false
        val forceNotNull = if (!arg.nullable) "!!" else ""
        code = when (className) {
            String::class.asTypeName() -> {
                nullable = arg.nullable
                CodeBlock.of("return requireArguments().getString(%S)%L", key, forceNotNull)
            }
            //Primitive type
            Short::class.asTypeName(), Int::class.asTypeName(), Long::class.asTypeName(),
            Float::class.asTypeName(), Double::class.asTypeName(),
            Byte::class.asTypeName(), Char::class.asTypeName() ->
                CodeBlock.of("return requireArguments().get${className.simpleName}(%S)", key)
            Boolean::class.asTypeName() ->{
                functionName = arg.name
                CodeBlock.of("return requireArguments().get${className.simpleName}(%S)", key)
            }
            CharSequence::class.asTypeName() -> {
                nullable = arg.nullable
                CodeBlock.of("return requireArguments().getCharSequence(%S)%L", key, forceNotNull)
            }
            else -> {
                nullable = arg.nullable
                //Support Serializable and Parcelable
                when {
                    className.getTypeElement(processingEnv).asType().isSubtypeOfType(Serializable::class.java.canonicalName) ->
                        CodeBlock.of(
                            "return requireArguments().getSerializable(%S) as %T",
                            key,
                            className.copy(arg.nullable)
                        )
                    className.getTypeElement(processingEnv).isSubtypeOfType(ANDROID_PARCELABLE) ->
                        CodeBlock.of(
                            "return requireArguments().getParcelable<%T>(%S) as %T",
                            className.copy(nullable),
                            key,
                            className.copy(arg.nullable)
                        )
                    else -> throw IllegalArgumentException("Type $className not support by ActivityArguments annotation")
                }
            }
        }
        if (nullable) className = className.copy(true) as ClassName
        return FunSpec.builder(functionName)
            .addKdoc(
                "This is extension method to get ${arg.name} from arguments of ${element.asClassName().simpleName}"
            )
            .receiver(element.asClassName())
            .addCode(code).returns(className).build()
    }
}