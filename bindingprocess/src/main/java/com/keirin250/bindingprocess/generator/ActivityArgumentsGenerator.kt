package com.keirin250.bindingprocess.generator


import com.google.auto.common.SuperficialValidation
import com.keirin250.annotations.ActivityArguments
import com.keirin250.annotations.ActivityNavModule
import com.keirin250.annotations.Parameter
import com.keirin250.bindingprocess.*
import com.squareup.kotlinpoet.*
import java.io.Serializable
import javax.annotation.processing.ProcessingEnvironment
import javax.annotation.processing.RoundEnvironment
import javax.lang.model.element.TypeElement
import kotlin.reflect.KClass

/**
 * Created by Thang Tran on 2019-10-03.
 * Copyright © 2019 Neolab SI6. All rights reserved.
 */
class ActivityArgumentsGenerator(roundEnv: RoundEnvironment, processingEnv: ProcessingEnvironment) :
    ArgumentsGenerator<ActivityArguments>(roundEnv, processingEnv) {

    override fun checkAndAddToList(): List<FileSpec> {
        checkProviderElement<ActivityNavModule>()
        val activityAnnotations = roundEnv
            .getElementsAnnotatedWith(getAnnotationClass().java)
        if (activityAnnotations.size == 0) return mutableListOf()
        for (element in activityAnnotations) {
            if (!SuperficialValidation.validateElement(element)) continue
            checkIsComponent(ANDROID_ACTIVITY, element)
            element as TypeElement
            //Check size parameter
            val args = checkAndGetParameters(element)

            addOrUpdateMap(element, buildNavOptionsClass(element, args))
            addOrUpdateMap(element, buildGetNavOptionFun(element, args))
            for (arg in args) {
                addOrUpdateMap(element, getFunctionGetValue(element, arg))
            }
        }

        return generateFileSpecBuilder()
    }

    private fun buildIntentFunc(
        element: TypeElement,
        args: Array<out Parameter>
    ): FunSpec {
        val funName = "buildIntent"
        val functionBuilder = FunSpec.builder(funName)
            .addModifiers(KModifier.PRIVATE)
        functionBuilder.addParameter(
            getIntentBlock()
        )
        val codeBlockBuilder = CodeBlock.builder()

        for (arg in args) {
            codeBlockBuilder.add(
                "intent.putExtra(%S, %L)\n",
                getArgumentKey(element, arg),
                arg.name
            )
        }
        codeBlockBuilder.add("block(intent)\n")
        functionBuilder.addCode(codeBlockBuilder.build())
        return functionBuilder.build()
    }

    private fun getIntentBlock(): ParameterSpec {
        return ParameterSpec.builder(
            "block",
            LambdaTypeName.get(receiver = ANDROID_INTENT, returnType = UNIT)
        ).defaultValue("{}").build()
    }

    private fun buildNavOptionsClass(
        element: TypeElement,
        args: Array<out Parameter>
    ): TypeSpec {
        val classBuilder = TypeSpec.classBuilder("${element.simpleName}NavOptions")
            .addKdoc("This is a NavOptions class to set argument, flags and navigate func")
        classBuilder.addProperty(
            PropertySpec.builder("from", ANDROID_CONTEXT).initializer("from")
                .addModifiers(KModifier.PRIVATE).build()
        )
        classBuilder.addProperty(
            PropertySpec.builder("intent", ANDROID_INTENT)
                .initializer("%T(from, %T::class.java)\n", ANDROID_INTENT, element.asClassName())
                .addModifiers(KModifier.PRIVATE).build()
        )
        val primaryConstructorBuilder = FunSpec.constructorBuilder()
            .addParameter("from", ANDROID_CONTEXT)
        val returnBuilderType =
            ClassName.bestGuess("${element.asClassName().packageName}.${element.simpleName}NavOptions")
        for (arg in args) {
            val type = getClassName(arg)
            if (!arg.nullable) {
                classBuilder.addProperty(
                    PropertySpec.builder(arg.name, type)
                        .initializer(arg.name)
                        .addModifiers(KModifier.PRIVATE)
                        .build()
                )
                primaryConstructorBuilder.addParameter(arg.name, type)
            } else {
                val typeCopy = type.copy(true)
                classBuilder.addProperty(
                    PropertySpec.builder(arg.name, typeCopy)
                        .initializer("null")
                        .mutable(true)
                        .addModifiers(KModifier.PRIVATE)
                        .build()
                )

                classBuilder.addFunction(
                    FunSpec.builder("set${arg.name.capitalize()}")
                        .addParameter(arg.name, typeCopy)
                        .addStatement("this.${arg.name} = ${arg.name}")
                        .returns(returnBuilderType)
                        .addStatement("return this")
                        .build()
                )

            }
        }

        //Add options clear top
        classBuilder.addFunction(
            FunSpec.builder("clearTop")
                .returns(returnBuilderType)
                .addStatement("intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)")
                .addStatement("return this")
                .build()
        )

        //Build fun navigate
        classBuilder.addFunction(
            FunSpec.builder("navigate")
                .addParameter(getIntentBlock())
                .addStatement("buildIntent(block)")
                .addStatement("from.startActivity(intent)")
                .build()
        )
        //Build fun navigate for result
        classBuilder.addFunction(
            FunSpec.builder("navigateForResult")
                .addParameter("requestCode", Int::class)
                .addParameter(getIntentBlock())
                .addStatement("buildIntent(block)")
                .addStatement(
                    "if(from is %T)from.startActivityForResult(intent, requestCode)",
                    ANDROID_ACTIVITY
                )
                .build()
        )
        //Build fun get Intent
        classBuilder.addFunction(buildIntentFunc(element, args))

        classBuilder.primaryConstructor(primaryConstructorBuilder.build())
        return classBuilder.build()
    }

    private fun buildGetNavOptionFun(element: TypeElement, args: Array<out Parameter>): FunSpec {
        val navOptionsClassName = "${element.asClassName().simpleName}NavOptions"
        val builder = FunSpec.builder("get$navOptionsClassName")
            .addKdoc("This fun, which is auto generated, provider to get NavOptions for navigate")
            .receiver(providerElement.asClassName())
            .returns(ClassName.bestGuess("${element.asClassName().packageName}.$navOptionsClassName"))
            .addParameter("from", ANDROID_CONTEXT)
        val stringCodeBlock = StringBuilder("$navOptionsClassName(from")
        args.forEach {
            if (!it.nullable) {
                val type = getClassName(it)
                builder.addParameter(it.name, type)
                stringCodeBlock.append(", ").append(it.name)
            }
        }
        stringCodeBlock.append(")")
        builder.addCode("return $stringCodeBlock")
        return builder.build()
    }


    override fun getAnnotationClass(): KClass<out ActivityArguments> {
        return ActivityArguments::class
    }

    override fun getFunctionGetValue(element: TypeElement, arg: Parameter): FunSpec {
        var functionName = "get${arg.name.capitalize()}"
        var className = getClassName(arg)
        val key = getArgumentKey(element, arg)
        val code: CodeBlock
        var nullable = false
        val forceNotNull = if (!arg.nullable) "!!" else ""
        code = when (className) {
            String::class.asTypeName() -> {
                nullable = arg.nullable
                CodeBlock.of("return intent.getStringExtra(%S)%L", key, forceNotNull)
            }
            Short::class.asTypeName() ->
                CodeBlock.of("return intent.getShortExtra(%S,0)", key)
            Int::class.asTypeName() ->
                CodeBlock.of("return intent.getIntExtra(%S, 0)", key)
            Long::class.asTypeName() ->
                CodeBlock.of("return intent.getLongExtra(%S, 0)", key)
            Float::class.asTypeName() ->
                CodeBlock.of("return intent.getFloatExtra(%S, 0.0F)", key)
            Double::class.asTypeName() ->
                CodeBlock.of("return intent.getDoubleExtra(%S, 0.0)", key)
            Boolean::class.asTypeName() -> {
                functionName = arg.name
                CodeBlock.of("return intent.getBooleanExtra(%S, false)", key)
            }
            Byte::class.asTypeName() ->
                CodeBlock.of("return intent.getByteExtra(%S, 0)", key)
            Char::class.asTypeName() ->
                CodeBlock.of("return intent.getCharExtra(%S, '-')", key)
            CharSequence::class.asTypeName() -> {
                nullable = arg.nullable
                CodeBlock.of("return intent.getCharSequenceExtra(%S)%L", key, forceNotNull)
            }
            else -> {
                nullable = arg.nullable
                //Support Serializable and Parcelable
                when {
                    className.getTypeElement(processingEnv).asType().isSubtypeOfType(Serializable::class.java.canonicalName) ->
                        CodeBlock.of(
                            "return intent.getSerializableExtra(%S) as %T",
                            key,
                            className.copy(arg.nullable)
                        )
                    className.getTypeElement(processingEnv).isSubtypeOfType(ANDROID_PARCELABLE) ->
                        CodeBlock.of(
                            "return intent.getParcelableExtra<%T>(%S)",
                            className.copy(nullable),
                            key
                        )
                    else -> throw IllegalArgumentException("Type $className not support by ActivityArguments annotation")
                }
            }
        }
        if (nullable) className = className.copy(true) as ClassName
        return FunSpec.builder(functionName)
            .addKdoc("Provider safe fun to get a ${arg.name} from ActivityArguments")
            .receiver(element.asClassName())
            .addCode(code).returns(className).build()
    }

}