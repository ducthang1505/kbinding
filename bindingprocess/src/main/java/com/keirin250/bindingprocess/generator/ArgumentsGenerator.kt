package com.keirin250.bindingprocess.generator

import com.keirin250.annotations.ActivityArguments
import com.keirin250.annotations.FragmentArguments
import com.keirin250.annotations.Parameter
import com.keirin250.bindingprocess.isSubtypeOfType
import com.keirin250.bindingprocess.javaToKotlinType
import com.squareup.kotlinpoet.*
import javax.annotation.processing.ProcessingEnvironment
import javax.annotation.processing.RoundEnvironment
import javax.lang.model.element.Element
import javax.lang.model.element.ElementKind
import javax.lang.model.element.TypeElement
import javax.lang.model.type.MirroredTypesException


/**
 * Created by Thang Tran on 2019-10-16.
 * Copyright © 2019 Neolab SI6. All rights reserved.
 */
abstract class ArgumentsGenerator<A : Annotation>(
    roundEnv: RoundEnvironment,
    processingEnv: ProcessingEnvironment
) : BaseGenerator<A>(roundEnv, processingEnv) {

    private val mapOfArgReceiver = mutableMapOf<TypeElement, List<Any>>()
    protected lateinit var providerElement: TypeElement

    protected fun addOrUpdateMap(typeElement: TypeElement, any: Any) {
        if (mapOfArgReceiver[typeElement] == null) {
            mapOfArgReceiver[typeElement] = mutableListOf(any)
        } else
            (mapOfArgReceiver[typeElement] as MutableList<Any>).add(any)
    }


    protected fun checkIsComponent(
        component: ClassName,
        element: Element
    ) {
        require(element.kind == ElementKind.CLASS && element.isSubtypeOfType(component)) {
            "Can only be applied to instance of ${component.simpleName} class,  element: $element "
        }
    }

    protected inline fun <reified ProviderAnnotation : Annotation> checkProviderElement() {
        val annotationClass = ProviderAnnotation::class
        val annotations =
            roundEnv.getElementsAnnotatedWith(annotationClass.java)

        if (annotations.size != 1 || annotations.first().kind != ElementKind.CLASS) {
            throw IllegalAccessException(
                """Should be provider @${annotationClass.simpleName} for empty singletone class in project. Ex: 
                    |@${annotationClass.simpleName} 
                    |object ProviderClass()
                """.trimMargin()
            )
        } else {
            providerElement = annotations.first() as TypeElement
        }
    }

    protected fun checkAndGetParameters(element: Element): Array<out Parameter> {
        when (val annotation = element.getAnnotation(getAnnotationClass().java)) {
            is ActivityArguments, is FragmentArguments -> {
                val args =
                    if (annotation is ActivityArguments) annotation.args else (annotation as FragmentArguments).args
                require(args.isNotEmpty()) {
                    "${element.simpleName} with ${getAnnotationClass()} must be at least one Parameter"
                }
                return args

            }
            else -> throw IllegalArgumentException("${element.simpleName} with ${getAnnotationClass()} must be @ActivityArguments or @FragmentArguments")
        }
    }

    protected fun generateFileSpecBuilder(): MutableList<FileSpec> {
        val files = mutableListOf<FileSpec>()
        for (item in mapOfArgReceiver) {
            val enclosingElement = item.key
            val items = item.value
            val className = ClassName.bestGuess("${enclosingElement.qualifiedName}Binding")
            val builder = FileSpec.builder(className.packageName, className.simpleName)
            for (item in items) {
                when (item) {
                    is FunSpec -> builder.addFunction(item)
                    is TypeSpec -> builder.addType(item)
                }

            }
            files.add(builder.build())
        }
        return files
    }

    protected fun getClassName(parameter: Parameter): ClassName {
        return try {
            parameter.type.asTypeName()
        } catch (ex: MirroredTypesException) {
            javaToKotlinType(ex.typeMirrors[0].asTypeName()) as ClassName
        }
    }

    protected fun getArgumentKey(
        element: TypeElement,
        arg: Parameter
    ) = "${element.asClassName().packageName}.ARG_${arg.name.toUpperCase()}"

    protected abstract fun getFunctionGetValue(element: TypeElement, arg: Parameter): FunSpec
}