package com.keirin250.bindingprocess.generator

import com.google.auto.common.SuperficialValidation
import com.keirin250.annotations.BindClick
import com.keirin250.bindingprocess.*
import com.squareup.kotlinpoet.*
import javax.annotation.processing.ProcessingEnvironment
import javax.annotation.processing.RoundEnvironment
import javax.lang.model.element.Element
import javax.lang.model.element.ElementKind
import javax.lang.model.element.ExecutableElement
import javax.lang.model.element.TypeElement
import kotlin.reflect.KClass


/**
 * Created by Thang Tran on 2019-10-02.
 * Copyright © 2019 Neolab SI6. All rights reserved.
 */
class BindClickGenerator(
    roundEnv: RoundEnvironment,
    processingEnv: ProcessingEnvironment
) : BaseGenerator<BindClick>(roundEnv, processingEnv) {
    companion object {
        private const val TARGET_PARAM_NAME = "target"
        private const val ROOT_VIEW_PARAM_NAME = "rootView"
        private const val HIDE_KB_ACTION = "hideKeyboardAction"
    }

    private val map = mutableMapOf<TypeElement, List<CodeBlock>>()

    /**
     * @return has error
     */
    override fun checkAndAddToList(): List<FileSpec> {
        var hasError = false
        val elementsAnnotatedWith = roundEnv
            .getElementsAnnotatedWith(getAnnotationClass().java)

        for (element in elementsAnnotatedWith) {
            if (!SuperficialValidation.validateElement(element)) continue
            if (element.kind != ElementKind.METHOD) {
                processingEnv.messager.errorMessage { "Can only be applied to functions,  element: $element " }
                hasError = true
            }
            element as ExecutableElement
            //Check size parameter, BindClick support less than one parameter
            if (element.parameters.size > 1) {
                processingEnv.messager.errorMessage {
                    "${getAnnotationClass()} support less than one parameter"
                }
                hasError = true
            }
            check(!hasError) { "BindClickGenerator checkAndAddToMap failed!" }

            val enclosingElement = element.enclosingElement as TypeElement
            addOrUpdateMap(enclosingElement, generatorCodeBlockForElement(element))
        }
        return generateFileSpecBuilder()
    }

    private fun addOrUpdateMap(typeElement: TypeElement, codeBlock: CodeBlock) {
        if (map[typeElement] == null) {
            map[typeElement] = mutableListOf(codeBlock)
        } else
            (map[typeElement] as MutableList<CodeBlock>).add(codeBlock)
    }

    private fun generatorCodeBlockForElement(funElement: Element): CodeBlock {
        val hasParam = (funElement as ExecutableElement).parameters.size == 1
        val viewGroupName =
            if (funElement.enclosingElement.isActivity()) TARGET_PARAM_NAME
            else ROOT_VIEW_PARAM_NAME
        val patternCode =
            """setOnClick(%L.findViewById<%T>(%L), %L){ %L.%L(**) }
            |""".trimMargin()
        val annotation = funElement.getAnnotation(getAnnotationClass().java)
        val hideBefore = annotation.hideKeyboardBefore
        if (hasParam) {
            val paramType = funElement.parameters[0].asType()
            return CodeBlock.of(
                patternCode.replace("**", "it as %T"),
                viewGroupName,
                paramType,
                annotation.viewId,
                hideBefore,
                TARGET_PARAM_NAME,
                funElement.simpleName,
                paramType
            )
        } else {
            return CodeBlock.of(
                patternCode.replace("**", ""),
                viewGroupName,
                ANDROID_VIEW,
                annotation.viewId,
                hideBefore,
                TARGET_PARAM_NAME,
                funElement.simpleName
            )
        }
    }

    private fun generateFileSpecBuilder(): List<FileSpec> {
        val files = mutableListOf<FileSpec>()
        for (item in map) {
            val (enclosingElement, codes) = item
            val className = ClassName.bestGuess("${enclosingElement.qualifiedName}Binding")
            val builder = FileSpec.builder(className.packageName, className.simpleName)
            val primaryConstructor = FunSpec.constructorBuilder()
                .addParameter(TARGET_PARAM_NAME, enclosingElement.asClassName())
            if (!enclosingElement.isActivity()) {
                primaryConstructor.addParameter(ROOT_VIEW_PARAM_NAME, ANDROID_VIEW_GROUP)
            }
            for (code in codes) {
                primaryConstructor.addCode(code)
            }
            val classSpec = TypeSpec.classBuilder(className.simpleName)
                .primaryConstructor(
                    primaryConstructor.build()
                )
                .addProperty(
                    PropertySpec.builder(
                        TARGET_PARAM_NAME,
                        enclosingElement.asClassName()
                    )
                        .initializer(TARGET_PARAM_NAME)
                        .addModifiers(KModifier.PRIVATE)
                        .build()
                )
                .addFunction(buildSetOnClickFun())
                .addFunction(buildHideKeyboardFun())
                .build()
            builder.addType(classSpec)
            files.add(builder.build())
        }
        return files
    }

    private fun buildSetOnClickFun(): FunSpec {
        return FunSpec.builder("setOnClick")
            .addModifiers(KModifier.PRIVATE, KModifier.INLINE)
            .addParameter("view", ANDROID_VIEW)
            .addParameter("hideBefore", Boolean::class)
            .addParameter(
                ParameterSpec(
                    "code",
                    LambdaTypeName.get(returnType = UNIT,
                        parameters = *arrayOf(ParameterSpec("view", ANDROID_VIEW))
                    ),
                    KModifier.CROSSINLINE
                )
            )
            .addCode(
                """view.setOnClickListener {
                    |   if (view.isClickable) {
                    |       view.isClickable = false
                    |       view.post { view.isClickable = true }
                    |       if (hideBefore) {
                    |           hideKeyboard(view)
                    |       }
                    |       code(view)
                    |       if (!hideBefore) {
                    |           hideKeyboard(view)
                    |       }
                    |   }   
                    |}""".trimMargin()
            )
            .build()
    }

    private fun buildHideKeyboardFun(): FunSpec {
        return FunSpec.builder("hideKeyboard")
            .addParameter("view", ANDROID_VIEW)
            .addCode(
                """val imm = view.context.getSystemService(%T.INPUT_METHOD_SERVICE) as %T
                    |           imm.hideSoftInputFromWindow(view.windowToken, 0)""".trimMargin(),
                ANDROID_CONTEXT,
                ANDROID_INPUT_METHOD_MANAGER
            )
            .build()
    }

    override fun getAnnotationClass(): KClass<BindClick> {
        return BindClick::class
    }
}