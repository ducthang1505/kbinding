package com.keirin250.bindingprocess.generator

import com.squareup.kotlinpoet.FileSpec
import javax.annotation.processing.ProcessingEnvironment
import javax.annotation.processing.RoundEnvironment
import kotlin.reflect.KClass


/**
 * Created by Thang Tran on 2019-10-01.
 * Copyright © 2019 Neolab SI6. All rights reserved.
 */
abstract class BaseGenerator<A : Annotation>(
    protected val roundEnv: RoundEnvironment,
    protected val processingEnv: ProcessingEnvironment
) {

    protected abstract fun checkAndAddToList() : List<FileSpec>

    abstract fun getAnnotationClass(): KClass<out A>

    fun generate(): List<FileSpec> {
       return checkAndAddToList()
    }
}