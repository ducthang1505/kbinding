package com.keirin250.bindingprocess

import com.google.auto.service.AutoService
import com.keirin250.bindingprocess.generator.ActivityArgumentsGenerator
import com.keirin250.bindingprocess.generator.BindClickGenerator
import com.keirin250.bindingprocess.generator.FragmentArgumentGenerator
import com.squareup.kotlinpoet.*
import net.ltgt.gradle.incap.IncrementalAnnotationProcessor
import net.ltgt.gradle.incap.IncrementalAnnotationProcessorType
import java.io.File
import javax.annotation.processing.*
import javax.lang.model.SourceVersion
import javax.lang.model.element.TypeElement


/**
 * Created by Thang Tran on 2019-10-01.
 * Copyright © 2019 Neolab SI6. All rights reserved.
 */
@AutoService(Processor::class)
@SupportedSourceVersion(SourceVersion.RELEASE_8)
@SupportedOptions(KAPT_KOTLIN_GENERATED_OPTION_NAME)
@IncrementalAnnotationProcessor(IncrementalAnnotationProcessorType.ISOLATING)
class BindingProcessor : AbstractProcessor() {

    override fun process(
        annotations: MutableSet<out TypeElement>,
        roundEnv: RoundEnvironment
    ): Boolean {
        if (annotations.size == 0) return true
        val binClickFiles = BindClickGenerator(roundEnv, processingEnv).generate()
        val activityArgsFiles = ActivityArgumentsGenerator(roundEnv, processingEnv).generate()
        val fragmentArgsFiles = FragmentArgumentGenerator(roundEnv, processingEnv).generate()
        val finalList = mergeListFile(binClickFiles, activityArgsFiles, fragmentArgsFiles)
        buildToFile(finalList)
        return false
    }

    //TODO rebuild all other types
    private fun mergeListFile(vararg listOfFiles: List<FileSpec>): List<FileSpec> {
        if (listOfFiles.size == 1) return listOfFiles.first()
        val flatList = mutableListOf<FileSpec>()
        for (item in listOfFiles) {
            flatList.addAll(item)
        }
        val groupById = flatList.groupBy { it.getId() }

        val finalList = mutableListOf<FileSpec>()
        for (item in groupById) {
            val key = item.key
            val files = item.value
            if (files.isEmpty()) break
            val fileName = ClassName.bestGuess(key)
            val builder = FileSpec.builder(fileName.packageName, fileName.simpleName)
            if (files.size == 1) {
                finalList.add(files.first())
                continue
            }
            for (file in files) {
                //Add top level properties
                file.members.filterIsInstance<PropertySpec>().forEach {
                    builder.addProperty(it)
                }
                //Add Class
                file.members.filterIsInstance<TypeSpec>().forEach {
                    //TODO check duplicate class
                    builder.addType(it)
                }
                //Add top level functions
                file.members.filterIsInstance<FunSpec>().forEach {
                    builder.addFunction(it)
                }
            }
            finalList.add(builder.build())
        }
        return finalList
    }


    private fun buildToFile(files: List<FileSpec>) {
        files.forEach { fileSpec ->
            val file = File(processingEnv.options[KAPT_KOTLIN_GENERATED_OPTION_NAME].orEmpty())
            file.mkdir()
            fileSpec.writeTo(file)
        }

    }

    override fun getSupportedAnnotationTypes(): MutableSet<String> {
        return supportTypes.map { it.java.canonicalName }.toMutableSet()
    }

}