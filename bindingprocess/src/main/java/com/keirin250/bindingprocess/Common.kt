package com.keirin250.bindingprocess

import com.keirin250.annotations.ActivityArguments
import com.keirin250.annotations.ActivityNavModule
import com.keirin250.annotations.BindClick
import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.FileSpec
import com.squareup.kotlinpoet.FunSpec
import com.squareup.kotlinpoet.TypeName
import javax.annotation.processing.Messager
import javax.annotation.processing.ProcessingEnvironment
import javax.lang.model.element.Element
import javax.lang.model.element.TypeElement
import javax.lang.model.type.DeclaredType
import javax.lang.model.type.TypeKind
import javax.lang.model.type.TypeMirror
import kotlin.reflect.KClass
import kotlin.reflect.jvm.internal.impl.builtins.jvm.JavaToKotlinClassMap
import kotlin.reflect.jvm.internal.impl.name.FqName


/**
 * Created by Thang Tran on 2019-10-01.
 * Copyright © 2019 Neolab SI6. All rights reserved.
 */

const val KAPT_KOTLIN_GENERATED_OPTION_NAME = "kapt.kotlin.generated"
val ANDROID_ACTIVITY = ClassName.bestGuess("android.app.Activity")
val ANDROID_CONTEXT = ClassName.bestGuess("android.content.Context")
val ANDROID_FRAGMENT = ClassName.bestGuess("android.app.Fragment")
val ANDROID_SUPPORT_FRAGMENT = ClassName.bestGuess("androidx.fragment.app.Fragment")
val ANDROID_INTENT = ClassName.bestGuess("android.content.Intent")
val ANDROID_PARCELABLE = ClassName.bestGuess("android.os.Parcelable")
val ANDROID_BUNDLE = ClassName.bestGuess("android.os.Bundle")
val ANDROID_VIEW = ClassName.bestGuess("android.view.View")
val ANDROID_VIEW_GROUP = ClassName.bestGuess("android.view.ViewGroup")
val ANDROID_INPUT_METHOD_MANAGER = ClassName.bestGuess("android.view.inputmethod.InputMethodManager")

val supportTypes = listOf<KClass<*>>(
    BindClick::class, ActivityArguments::class,
    ActivityNavModule::class
)

fun Messager.errorMessage(message: () -> String) {
    this.printMessage(javax.tools.Diagnostic.Kind.ERROR, message())
}

fun Messager.noteMessage(message: () -> String) {
    this.printMessage(javax.tools.Diagnostic.Kind.NOTE, message())
}

fun Messager.warningMessage(message: () -> String) {
    this.printMessage(javax.tools.Diagnostic.Kind.WARNING, message())
}

fun javaToKotlinType(type: TypeName): TypeName {
    val className =
        JavaToKotlinClassMap.INSTANCE.mapJavaToKotlin(FqName(type.toString()))
            ?.asSingleFqName()?.asString()

    return if (className == null) {
        type
    } else {
        ClassName.bestGuess(className)
    }
}

internal fun Element.isSubtypeOfType(otherType: ClassName): Boolean {
    return asType().isSubtypeOfType(otherType.canonicalName)
}

internal fun TypeMirror.isSubtypeOfType(otherType: String): Boolean {
    if (this.toString() == otherType) {
        return true
    }
    if (this.kind != TypeKind.DECLARED) {
        return false
    }
    val declaredType = this as DeclaredType
    val typeArguments = declaredType.typeArguments
    if (typeArguments.size > 0) {
        val typeString = StringBuilder(declaredType.asElement().toString())
        typeString.append('<')
        for (i in typeArguments.indices) {
            if (i > 0) {
                typeString.append(',')
            }
            typeString.append('?')
        }
        typeString.append('>')
        if (typeString.toString() == otherType) {
            return true
        }
    }
    val element = declaredType.asElement()
    if (element !is TypeElement) {
        return false
    }
    val superType = element.superclass
    if (superType.isSubtypeOfType(otherType)) {
        return true
    }
    for (interfaceType in element.interfaces) {
        if (interfaceType.isSubtypeOfType(otherType)) {
            return true
        }
    }
    return false
}

fun ClassName.getTypeElement(processingEnv: ProcessingEnvironment): TypeElement {
    return processingEnv.elementUtils.getTypeElement(canonicalName)
}

fun getTypeElement(name: String, processingEnv: ProcessingEnvironment): TypeElement {
    return processingEnv.elementUtils.getTypeElement(name)
}

fun FileSpec.Builder.getId(): String {
    return "$packageName.$name"
}

fun FileSpec.getId(): String {
    return "$packageName.$name"
}

fun Element.isFragment(): Boolean {
    return isSubtypeOfType(ANDROID_SUPPORT_FRAGMENT) || isSubtypeOfType(ANDROID_FRAGMENT)
}

fun Element.isActivity(): Boolean {
    return isSubtypeOfType(ANDROID_ACTIVITY)
}

val FunSpec.Companion.COMPANION_OBJECT : String
    get() = "Companion"
